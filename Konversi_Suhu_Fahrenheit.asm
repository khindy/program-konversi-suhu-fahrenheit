NAME "KONVERSI SUHU FAHRENHEIT KE CELSIUS"
INCLUDE 'emu8086.inc'   ;memanggil library emu8086.inc
#MAKE_COM#              ;untuk membuat file com
ORG 100H                ;mulai dari 100H
JMP MULAI               ;lompat ke MULAI

PSN1    DB "-----------------------------------",13,10
        DB "KONVERSI SUHU FAHRENHEIT KE CELSIUS",13,10 
        DB "-----------------------------------",13,10
        DB "===================================",13,10,'$'
PSN2    DB 13,10,"Masukkan Suhu Dalam Fahrenheit: $"
PSN3    DB 13,10,"Skala Derajat Celsius : $ "
PSN4    DB "Skala Derajat Kelvin  : $ "
PSN5    DB "Skala Derajat Reamur  : $ "        
PSN6    DB 13,10,"--------------------------"
        DB 13,10,13,10,"SELESAI"
        DB 13,10,"TEKAN APA SAJA UNTUK KELUAR $" 
        
CEL     DW ?            ;variabel celsius
KEL     DW ?            ;variabel kelvin
REA     DW ?            ;variabel reaumur 
FAH     DW ?            ;variabel 

MULAI:
LEA DX,PSN1             ;memuat alamat PSN1 pada DX
MOV AH,9                ;interupsi mencetak STRING PSN1
INT 21H                 

LEA DX,PSN2
MOV AH,9                ;interupsi mencetak STRING PSN2
INT 21H

CALL SCAN_NUM           ;memanggil prosedur SCAN_NUM

MOV FAH,CX              ;menyimpan nilai derajat fahrenheit dalam variabel FAH
                        

PUTC 13                 ;berganti baris selanjutnya
PUTC 10

LEA DX,PSN3
MOV AH,9                ;interupsi  STRING PSN3
INT 21H

MOV AX,FAH              ;konversi fahrenheit menjadi celsius
SUB AX,32               ;CELSIUS=(FAHRENHEIT - 32)*5/9
MOV BX,5
IMUL BX
MOV BX,9
IDIV BX

MOV CEL,AX              ;menyimpan nilai celsius dalam variabel CEL                      
CALL PRINT_NUM          ;memanggil prosedur PRINT_NUM
                        ;mencetak nilai AX
                      
PUTC 13                 ;berganti ke baris selanjutnya
PUTC 10

LEA DX,PSN4
MOV AH,9                ;interupsi mencetak STRING PSN4
INT 21H

MOV AX,FAH              ;konversi fahrenheit menjadi kelvin
SUB AX,32               ;rumus KELVIN=(FAHRENHEIT-32)*5/9+273.15
MOV BX,5                
IMUL BX
MOV BX,9
IDIV BX
ADD AX,273

MOV KEL,AX              ;menyimpan nilai kelvin dalam variabel KEL
CALL PRINT_NUM          ;memanggil prosedur PRINT_NUM
                        ;mencetak nilai AX
                  
PUTC 13                 ;berganti ke baris selanjutnya
PUTC 10

LEA DX,PSN5
MOV AH,9                ;interupsi mencetak STRING PSN5
INT 21H

MOV AX,FAH              ;konversi fahrenheit ke reamur
SUB AX,32               ;rumus : REAMUR=(F - 32)* 0.44444
MOV BX,100              ;alternatif : REAMUR=(F-32)*100/225
IMUL BX
MOV BX,225
IDIV BX

MOV REA,AX              ;menyimpan nilai reamur dalam variabel REA                 
CALL PRINT_NUM          ;memanggil prosedur PRINT_NUM
                        ;mencetak nilai AX
                     
LEA DX,PSN6
MOV AH,9                ;interupsi untuk mencetak STRING PSN6
INT 21H

MOV AH,0                ;interupsi untuk menunggu
INT 16H

RET                     ;kembali/return

DEFINE_SCAN_NUM         ;mendifinisikan prosedur
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS
END                     ;selesai/ending
